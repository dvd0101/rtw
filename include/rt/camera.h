#pragma once
#include "./geo.h"
#include "./random.h"
#include <glm/geometric.hpp>
#include <glm/trigonometric.hpp>
#include <optional>

namespace rt {
    class camera {
      public:
        struct camera_init {
            glm::vec3 look_from;
            glm::vec3 look_at;
            glm::vec3 vup = {0, 1, 0};
            float vertical_fov = 90.f;
            float aspect_ratio = 16.f / 9.f;
            float aperture = 0.f;
            std::optional<float> focus_distance = {};
        };

        camera(camera_init args);

        const glm::vec3& origin() const { return origin_; }

        ray cast_ray(float s, float t) const;

      private:
        glm::vec3 origin_;

        glm::vec3 top_left_;
        glm::vec3 horz_;
        glm::vec3 vert_;

        glm::vec3 u_, v_, w_;

        float lens_radius_;
    };
}
