#pragma once
#include <glm/gtx/norm.hpp>
#include <glm/vec3.hpp>
#include <random>

namespace rt {
    inline float get_rand() {
        static std::mt19937_64 generator;
        static std::uniform_real_distribution<float> distribution(0.0, 1.0);
        return distribution(generator);
    }

    inline float get_rand(float min, float max) { return min + (max - min) * get_rand(); }

    inline glm::vec3 random_vect() { return {get_rand(), get_rand(), get_rand()}; }
    inline glm::vec3 random_vect(float min, float max) {
        return {get_rand(min, max), get_rand(min, max), get_rand(min, max)};
    }

    inline glm::vec3 random_point_in_unit_sphere() {
        glm::vec3 pt;
        while (true) {
            pt.x = get_rand(-1, 1);
            pt.y = get_rand(-1, 1);
            pt.z = get_rand(-1, 1);
            if (length2(pt) <= 1)
                break;
        }
        return pt;
    }

    inline glm::vec3 random_unit_vector() { return normalize(random_point_in_unit_sphere()); }
}
