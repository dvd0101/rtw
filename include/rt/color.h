#pragma once
#include <algorithm>
#include <cmath>
#include <glm/vec4.hpp>

namespace rt {
    class color : public glm::vec4 {
      public:
        using glm::vec4::vec4;

        using glm::vec4::operator+=;
    };
    static_assert(sizeof(color) == sizeof(float) * 4);

    inline color operator*(const color& a, const color& b) {
        auto r = static_cast<glm::vec4>(a) * static_cast<glm::vec4>(b);
        return {r.x, r.y, r.z, r.w};
    }

    inline color operator*(float a, const color& b) {
        auto r = a * static_cast<glm::vec4>(b);
        return {r.x, r.y, r.z, r.w};
    }

    inline color operator+(const color& a, const color& b) {
        auto r = static_cast<glm::vec4>(a) + static_cast<glm::vec4>(b);
        return {r.x, r.y, r.z, r.w};
    }

    inline color gamma2(color c) {
        c.x = std::sqrt(c.x);
        c.y = std::sqrt(c.y);
        c.z = std::sqrt(c.z);
        c.w = std::sqrt(c.w);
        return c;
    }

    struct rgba {
        uint8_t r, g, b, a;
    };
    static_assert(sizeof(rgba) == 4);

    inline rgba to_rgba(const color& v) {
        return {
            .r = (uint8_t)std::llround(std::clamp(v.x, 0.f, 1.f) * 255),
            .g = (uint8_t)std::llround(std::clamp(v.y, 0.f, 1.f) * 255),
            .b = (uint8_t)std::llround(std::clamp(v.z, 0.f, 1.f) * 255),
            .a = (uint8_t)std::llround(std::clamp(v.w, 0.f, 1.f) * 255),
        };
    }

}
