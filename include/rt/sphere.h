#pragma once
#include "./interfaces.h"

namespace rt {
    struct sphere : public hittable {
        sphere(glm::vec3 c, float r, material* m) : center{c}, radius{r}, mat{m} {}

        std::optional<hittable::target> hit(const ray& r, interval ti) const override;

        glm::vec3 center;
        float radius;
        material* mat;
    };
}
