#pragma once
#include "./geo.h"
#include <glm/geometric.hpp>
#include <optional>

namespace rt {
    class material;

    class hittable {
      public:
        struct target {
            glm::vec3 pt;
            glm::vec3 n;
            float t;
            bool front_face;
            material* mat;
        };

        virtual std::optional<target> hit(const ray& r, interval ti) const = 0;

        std::pair<glm::vec3, bool> determine_face_normal(const ray& r, const glm::vec3& outward_normal) const {
            bool front_face = dot(r.direction(), outward_normal) < 0;
            return {front_face ? outward_normal : -outward_normal, front_face};
        }
    };

    class material {
      public:
        struct scatter_info {
            glm::vec3 attenuation;
            ray scattered;
        };

        virtual std::optional<scatter_info> scatter(const ray& r, const hittable::target& hit) const = 0;
    };
}
