#pragma once
#include <cmath>
#include <glm/vec3.hpp>
#include <limits>
#include <numbers>

namespace rt {
    const float pi = std::numbers::pi;
    const float inf = std::numeric_limits<float>::infinity();

    struct size {
        float w, h;
    };

    struct interval {
        float min, max;
    };

    class ray {
      public:
        ray(const glm::vec3& o, const glm::vec3& d) : orig{o}, dir{d} {}

        const glm::vec3& origin() const { return orig; }
        const glm::vec3& direction() const { return dir; }

        glm::vec3 at(float t) const { return orig + t * dir; }

      private:
        glm::vec3 orig;
        glm::vec3 dir;
    };

    /// Return true if the vector is close to zero in all dimensions.
    inline bool near_zero(const glm::vec3& v) {
        const auto s = 1e-8;
        return (std::abs(v.x) < s) && (std::abs(v.y) < s) && (std::abs(v.y) < s);
    }
}
