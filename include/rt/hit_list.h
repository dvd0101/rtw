#pragma once
#include "./interfaces.h"
#include <vector>

namespace rt {
    class hit_list : public hittable {
      public:
        void add(const hittable* ptr) { objects_.push_back(ptr); }
        void clear() { objects_.clear(); }

        std::optional<hittable::target> hit(const ray& r, interval ti) const override;

      private:
        std::vector<const hittable*> objects_;
    };
}
