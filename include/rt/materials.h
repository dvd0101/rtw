#pragma once
#include "./interfaces.h"

namespace rt {
    class lambertian : public material {
      public:
        lambertian(const glm::vec3& color) : albedo(color) {}

        std::optional<scatter_info> scatter(const ray&, const hittable::target& hit) const override;

      public:
        glm::vec3 albedo;
    };

    class metal : public material {
      public:
        metal(const glm::vec3& color, float fuzz = 0);

        std::optional<scatter_info> scatter(const ray& r, const hittable::target& hit) const override;

      public:
        glm::vec3 albedo;
        float fuzz;
    };

    class dielectric : public material {
      public:
        dielectric(double index_of_refraction) : ir(index_of_refraction) {}

        std::optional<scatter_info> scatter(const ray& r, const hittable::target& hit) const override;

      public:
        // Index of Refraction
        float ir;

      private:
        // Use Schlick's approximation for reflectance.
        static float reflectance(float cosine, float ref_idx);
    };
}
