#pragma once
#include "./color.h"
#include "./interfaces.h"

namespace rt {
    color ray_color(const ray& r, const hittable& world, uint32_t depth);

    class camera;

    std::vector<rgba> trace(const camera& cam, int width, int height, const hittable& world, int samples = 100);
}
