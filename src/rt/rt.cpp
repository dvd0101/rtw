#include <iostream>
#include <rt/camera.h>
#include <rt/random.h>
#include <rt/rt.h>

namespace rt {
    color ray_color(const ray& r, const hittable& world, uint32_t depth) {
        if (depth == 0)
            return {0, 0, 0, 1};

        if (auto t = world.hit(r, {0.001, inf}); t.has_value()) {
            auto scatter = t->mat->scatter(r, *t);
            if (scatter) {
                auto [attenuation, scatter_direction] = *scatter;
                return color{attenuation.x, attenuation.y, attenuation.z, 1.}
                       * ray_color(scatter_direction, world, depth - 1);
            } else {
                return {0, 0, 0, 1};
            }
        }

        auto unit = glm::normalize(r.direction());
        float t = 0.5f * (unit.y + 1.0);
        return (1.0f - t) * color(1., 1., 1., 1.) + t * color(0.5, 0.7, 1., 1.);
    }

    std::vector<rgba> trace(const camera& cam, int width, int height, const hittable& world, int samples) {
        const uint32_t max_depth = 50;

        std::vector<rt::rgba> pixels;
        pixels.reserve(width * height);

        for (int y = 0; y < height; y++) {
            std::cout << "scanline " << y << "\n";
            for (int x = 0; x < width; x++) {

                rt::color pixel_color(0, 0, 0, 0);
                for (int sample = 0; sample < samples; sample++) {
                    const float v = (y + get_rand()) / (height - 1.f);
                    const float u = (x + get_rand()) / (width - 1.f);
                    pixel_color += ray_color(cam.cast_ray(u, v), world, max_depth);
                }

                pixel_color /= samples;
                pixels.push_back(to_rgba(rt::gamma2(pixel_color)));
            }
        }

        return pixels;
    }
}
