#include <rt/hit_list.h>
namespace rt {
    std::optional<hittable::target> hit_list::hit(const ray& r, interval ti) const {
        std::optional<hittable::target> result;

        for (auto ptr : objects_) {
            if (auto h = ptr->hit(r, ti); h) {
                result = *h;
                ti.max = result->t;
            }
        }

        return result;
    }
}
