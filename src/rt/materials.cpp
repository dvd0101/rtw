#include <rt/materials.h>
#include <rt/random.h>

namespace rt {

    std::optional<material::scatter_info> lambertian::scatter(const ray&, const hittable::target& hit) const {
        auto scatter_direction = hit.n + random_unit_vector();
        if (near_zero(scatter_direction))
            scatter_direction = hit.n;

        return scatter_info{
            .attenuation = albedo,
            .scattered = ray(hit.pt, scatter_direction),
        };
    }

}

namespace rt {

    metal::metal(const glm::vec3& color, float fuzz) : albedo(color), fuzz{std::clamp(fuzz, 0.f, 1.f)} {}

    std::optional<material::scatter_info> metal::scatter(const ray& r, const hittable::target& hit) const {
        glm::vec3 reflected = reflect(normalize(r.direction()), hit.n);
        scatter_info si{
            .attenuation = albedo,
            .scattered = ray(hit.pt, reflected + fuzz * random_point_in_unit_sphere()),
        };
        if (dot(si.scattered.direction(), hit.n) > 0) {
            return si;
        }
        return {};
    }

}

namespace rt {

    std::optional<material::scatter_info> dielectric::scatter(const ray& r, const hittable::target& hit) const {
        float refraction_ratio = hit.front_face ? (1.0 / ir) : ir;

        auto unit_direction = normalize(r.direction());

        const float cos_theta = fmin(dot(-unit_direction, hit.n), 1.0);
        const float sin_theta = sqrt(1.0 - cos_theta * cos_theta);

        glm::vec3 scattered_direction;
        if (refraction_ratio * sin_theta > 1 || reflectance(cos_theta, refraction_ratio) > get_rand()) {
            // reflect
            scattered_direction = glm::reflect(unit_direction, hit.n);
        } else {
            // refract
            scattered_direction = glm::refract(unit_direction, hit.n, refraction_ratio);
        }

        return scatter_info{
            .attenuation = {1.f, 1.f, 1.f},
            .scattered = ray(hit.pt, scattered_direction),
        };
    }

    float dielectric::reflectance(float cosine, float ref_idx) {
        auto r0 = (1 - ref_idx) / (1 + ref_idx);
        r0 = r0 * r0;
        return r0 + (1 - r0) * pow((1 - cosine), 5);
    }
}
