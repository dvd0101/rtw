#include <glm/gtx/norm.hpp>
#include <rt/sphere.h>

namespace rt {
    std::optional<hittable::target> sphere::hit(const ray& r, interval ti) const {
        const glm::vec3 oc = r.origin() - center;
        const auto a = glm::length2(r.direction());
        const auto hb = dot(oc, r.direction());
        const auto c = glm::length2(oc) - radius * radius;

        const auto discriminant = hb * hb - a * c;
        if (discriminant < 0)
            return {};

        const auto sd = std::sqrt(discriminant);

        auto sol = (-hb - sd) / a;
        if (sol < ti.min || sol > ti.max) {
            sol = (-hb + sd) / a;
        }
        if (sol < ti.min || sol > ti.max) {
            return {};
        }

        glm::vec3 pt = r.at(sol);
        glm::vec3 outward_normal = (pt - center) / radius;
        auto [n, front_face] = determine_face_normal(r, outward_normal);
        return hittable::target{
            .pt = pt,
            .n = n,
            .t = sol,
            .front_face = front_face,
            .mat = mat,
        };
    }
}
