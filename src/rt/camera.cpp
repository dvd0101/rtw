#include <rt/camera.h>

namespace rt {
    camera::camera(camera_init args) : origin_{args.look_from} {
        const float theta = glm::radians(args.vertical_fov);
        const auto vw_half_height = std::tan(theta / 2);

        size viewport;
        viewport.h = 2 * vw_half_height;
        viewport.w = viewport.h * args.aspect_ratio;

        w_ = normalize(args.look_from - args.look_at);
        u_ = normalize(cross(args.vup, w_));
        v_ = cross(w_, u_);

        const float focus_distance = args.focus_distance.value_or(length(args.look_from - args.look_at));

        horz_ = focus_distance * viewport.w * u_;
        vert_ = focus_distance * viewport.h * v_;

        using glm::vec3;
        top_left_ = origin_ - horz_ / 2.f + vert_ / 2.f - focus_distance * w_;

        lens_radius_ = args.aperture / 2.f;
    }

    ray camera::cast_ray(float s, float t) const {
        auto rd = lens_radius_ * random_point_in_unit_sphere();
        auto offset = u_ * rd.x + v_ * rd.y;

        return ray(origin_ + offset, top_left_ + s * horz_ - t * vert_ - origin_ - offset);
    }
}
