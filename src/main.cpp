#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp>
#include <iostream>
#include <stb/stb_image_write.h>
#include <vector>

#include <rt/camera.h>
#include <rt/color.h>
#include <rt/hit_list.h>
#include <rt/interfaces.h>
#include <rt/materials.h>
#include <rt/random.h>
#include <rt/rt.h>
#include <rt/sphere.h>

struct world {
    std::vector<std::unique_ptr<rt::material>> materials;
    std::vector<std::unique_ptr<rt::hittable>> objects;
};

world construct() {
    world w;
    using namespace rt;
    // ground
    w.materials.emplace_back(new lambertian({0.5, 0.5, 0.5}));
    w.objects.emplace_back(new sphere({0, -1000, 0}, 1000, w.materials[0].get()));

    for (int a = -11; a < 11; a++) {
        for (int b = -11; b < 11; b++) {
            auto choose_mat = get_rand();
            glm::vec3 center(a + 0.9 * get_rand(), 0.2, b + 0.9 * get_rand());

            if ((center - glm::vec3(4, 0.2, 0)).length() > 0.9) {
                std::unique_ptr<material> sphere_material;

                if (choose_mat < 0.8) {
                    // diffuse
                    auto albedo = random_vect() * random_vect();
                    sphere_material.reset(new lambertian(albedo));
                } else if (choose_mat < 0.95) {
                    // metal
                    auto albedo = random_vect(0.5, 1);
                    auto fuzz = get_rand(0, 0.5);
                    sphere_material.reset(new metal(albedo, fuzz));
                } else {
                    // glass
                    sphere_material.reset(new dielectric(1.5));
                }
                w.objects.emplace_back(new sphere(center, 0.2, sphere_material.get()));
                w.materials.push_back(std::move(sphere_material));
            }
        }
    }

    auto material1 = std::make_unique<dielectric>(1.5);
    w.objects.emplace_back(new sphere({0, 1, 0}, 1.0, material1.get()));

    auto material2 = std::make_unique<lambertian>(glm::vec3{0.4, 0.2, 0.1});
    w.objects.emplace_back(new sphere({-4, 1, 0}, 1.0, material2.get()));

    auto material3 = std::make_unique<metal>(glm::vec3{0.7, 0.6, 0.5}, 0.0);
    w.objects.emplace_back(new sphere({4, 1, 0}, 1.0, material3.get()));
    w.materials.push_back(std::move(material1));
    w.materials.push_back(std::move(material2));
    w.materials.push_back(std::move(material3));

    return w;
}

world construct_small() {
    using namespace rt;

    world w;
    std::vector<std::unique_ptr<material>> materials;
    w.materials.emplace_back(new lambertian({0.8, 0.8, 0}));
    w.materials.emplace_back(new lambertian({0.1, 0.2, 0.5}));
    w.materials.emplace_back(new dielectric(1.5));
    w.materials.emplace_back(new metal({0.8, 0.6, 0.2}, 0));

    std::vector<std::unique_ptr<sphere>> objects;
    w.objects.emplace_back(new sphere({0, -100.5, -1}, 100, w.materials[0].get()));
    w.objects.emplace_back(new sphere({0, 0, -1}, 0.5, w.materials[1].get()));
    w.objects.emplace_back(new sphere({-1, 0, -1}, 0.5, w.materials[2].get()));
    w.objects.emplace_back(new sphere({-1, 0, -1}, -0.4, w.materials[2].get()));
    w.objects.emplace_back(new sphere({1, 0, -1}, 0.5, w.materials[3].get()));

    return w;
}

int main() {
    rt::camera::camera_init cam_args;

    world w;
    if (0) {
        w = construct_small();
        cam_args = {
            .look_from = {-2, 2, 1},
            .look_at = {0, 0, -1},
            .vertical_fov = 20.f,
            .aspect_ratio = 16.f / 9.f,
            .aperture = 0.1,
        };
    } else {
        w = construct();
        cam_args = {
            .look_from = {13, 2, 3},
            .look_at = {0, 0, 0},
            .vertical_fov = 20,
            .aspect_ratio = 3.f / 2.f,
            .aperture = 0.1,
            .focus_distance = 10.f,
        };
    }

    const int width = 400;
    const int height = width / cam_args.aspect_ratio;
    const int samples = 100;

    rt::camera cam(cam_args);

    rt::hit_list all_objects;
    for (auto&& obj : w.objects)
        all_objects.add(obj.get());

    std::vector<rt::rgba> pixels = rt::trace(cam, width, height, all_objects, samples);

    stbi_write_bmp("./x.bmp", width, height, 4, pixels.data());
}
